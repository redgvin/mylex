(function(){
	let imgInp = document.querySelector("#imgInp");
	let form = document.querySelector("form");
	let colMd9 = document.querySelector(".col-md-9");
	let emptyAvatar = document.querySelector(".empty-avatar");
	let requireWidth  = 300;
	let requireHeight = 300;

	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
				form.style.display = "none";
				colMd9.style.display = "block";
				cropImg(e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	emptyAvatar.addEventListener('click', function() {
	  	imgInp.click();
	});

	imgInp.addEventListener('change', function() {
	  	readURL(this);
	});


	function cropImg (src) {
		let dragStart = false;
		let cropMove = document.querySelector('.crope-move');
		let cropDragBox = document.querySelector('.crop-drag-box');
		let cropBox = document.querySelector('.crop-box');
		let canvasImg = document.querySelector('.crop-canvas img');
		let cropViewImg = document.querySelector('.crop-view img');
		
		canvasImg.setAttribute('src', src);
		cropViewImg.setAttribute('src', src);
		
		let cropContainer = document.querySelector('.crop-container');
		let plus = document.querySelector('.plus');
		let minus = document.querySelector('.minus');
		let left = document.querySelector('.left');
		let right = document.querySelector('.right');
		let down = document.querySelector('.down');
		let up = document.querySelector('.up');
		let done = document.querySelector('.done');
		let cropDragBoxCoordinates = cropDragBox.getBoundingClientRect();
		let cropBoxCordinates = cropBox.getBoundingClientRect();
		let clientX = 0;
		let clientY = 0;
		let translateX = 0;
		let translateY = 0;
		let scaleUp = 1/0.9;
		let scaleDown = 0.9
		let deltaVertical = canvasImg.naturalHeight * 0.05;
		let deltaHorisontal = canvasImg.naturalWidth * 0.05;
		let offsetX = 0;
		let offsetY = 0;
		let deltaCropBoxX = 250;
		let deltaCropBoxY = 100;

		canvasImg.style.width = "100%";
		cropViewImg.style.width = canvasImg.offsetWidth + 'px';
		cropViewImg.style.transform = 'translateX(-' +deltaCropBoxX+ 'px)';
		cropViewImg.style.transform += ' translateY(-'+deltaCropBoxY+'px)';

		cropMove.addEventListener("mousedown", function( event ) {
			clientX = event.clientX;
			clientY = event.clientY;

			dragStart = true;
		}, false);


		document.addEventListener("mouseup", function( event ) {
			offsetX = translateX;
			offsetY = translateY;
			dragStart = false;
		}, false);

		document.addEventListener("mousemove", function( event ) {
			if (dragStart) {
				let newTop = translateY;
				let newLeft = translateX;
				
				if (event.clientY < cropDragBoxCoordinates.bottom && event.clientY > cropDragBoxCoordinates.top) {
					newTop = offsetY + event.clientY - clientY;
					translateY = newTop;
				}
				if (event.clientX < cropDragBoxCoordinates.right && event.clientX > cropDragBoxCoordinates.left) {
					newLeft = offsetX + event.clientX - clientX;
					translateX = newLeft;
				}

				canvasImg.style.transform = 'translateX(' + newLeft + 'px) translateY(' + newTop + 'px)';
				cropViewImg.style.transform = 'translateX(' + (newLeft - deltaCropBoxX) + 'px) translateY(' + (newTop - deltaCropBoxY) + 'px)';
			}
	  	}, false);

	  	document.addEventListener("mousewheel", function( event ) {
	  		event.preventDefault();
	  		if (event.wheelDelta > 0) {
	  			canvasImg.style.width = canvasImg.offsetWidth * scaleDown + 'px';
	  			cropViewImg.style.width = cropViewImg.offsetWidth * scaleDown + 'px';
	  		} else {
	  			canvasImg.style.width = canvasImg.offsetWidth * scaleUp + 'px';
	  			cropViewImg.style.width = cropViewImg.offsetWidth * scaleUp + 'px';
	  		}

	  	});

	  	plus.addEventListener("click", function( event ) {
	  		canvasImg.style.width = canvasImg.offsetWidth * scaleUp + 'px';
	  		cropViewImg.style.width = cropViewImg.offsetWidth * scaleUp + 'px';
	  	});

	  	minus.addEventListener("click", function( event ) {
	  		canvasImg.style.width = canvasImg.offsetWidth * scaleDown + 'px';
	  		cropViewImg.style.width = cropViewImg.offsetWidth * scaleDown + 'px';

	  	});

	  	up.addEventListener("click", function( event ) {
			canvasImg.style.transform = 'translateX(' + translateX + 'px) translateY(' + (translateY - deltaVertical) + 'px)';
			cropViewImg.style.transform = 'translateX(' + (translateX - deltaCropBoxX) + 'px) translateY(' + (translateY - deltaVertical - deltaCropBoxY) + 'px)';
			translateY = translateY - deltaVertical;
	  	});
	  	
	  	down.addEventListener("click", function( event ) {
			canvasImg.style.transform = 'translateX(' + translateX + 'px) translateY(' + (translateY + deltaVertical) + 'px)';
			cropViewImg.style.transform = 'translateX(' + (translateX - deltaCropBoxX) + 'px) translateY(' + (translateY + deltaVertical - deltaCropBoxY) + 'px)';
			translateY = translateY + deltaVertical;

	  	});

	  	right.addEventListener("click", function( event ) {
			canvasImg.style.transform = 'translateX(' + (translateX + deltaHorisontal) + 'px) translateY(' + translateY + 'px)';
			cropViewImg.style.transform = 'translateX(' + (translateX + deltaHorisontal - deltaCropBoxX) + 'px) translateY(' + (translateY - deltaCropBoxY) + 'px)';
			translateX = translateX + deltaHorisontal;
	  	});
	  	
	  	left.addEventListener("click", function( event ) {
			canvasImg.style.transform = 'translateX(' + (translateX - deltaHorisontal) + 'px) translateY(' + translateY + 'px)';
			cropViewImg.style.transform = 'translateX(' + (translateX - deltaHorisontal - deltaCropBoxX) + 'px) translateY(' + (translateY - deltaCropBoxY) + 'px)';
			translateX = translateX - deltaHorisontal;

	  	});

	  	done.addEventListener("click", function( event ) {
	  		let widthRatio = canvasImg.naturalHeight / canvasImg.offsetHeight;
	  		let heightRatio = canvasImg.naturalWidth / canvasImg.offsetWidth;
			let canvasImgCoordinates = canvasImg.getBoundingClientRect();
	    	let canvas = document.getElementById('canvas');
	    	let ctx = canvas.getContext('2d');
	    	let startX = (cropBoxCordinates.left - canvasImgCoordinates.left) * widthRatio;
	    	let startY = (cropBoxCordinates.top - canvasImgCoordinates.top) * heightRatio;
	    	let getWidth = cropBoxCordinates.width * widthRatio;
	    	let getHeight = cropBoxCordinates.height * heightRatio;

			ctx.clearRect(0, 0, canvas.width, canvas.height);
		    ctx.drawImage(canvasImg,
		        startX, startY,   // Start at 70/20 pixels from the left and the top of the image (crop),
		        getWidth, getHeight,   // "Get" a `50 * 50` (w * h) area from the source image (crop),
		        0, 0,     // Place the result at 0, 0 in the canvas,
		        requireWidth, requireHeight); // With as width / height: 100 * 100 (scale)

		   	let dataURL = canvas.toDataURL();
			emptyAvatar.src = dataURL;
			
			emptyAvatar.onload = function(e) {
				form.style.display = "block";
				colMd9.style.display = "none";
				
				//send to server
				//send(dataURL)
			}
	  	});
	}
	
	function send(data) {
		let xhr = new XMLHttpRequest();

		xhr.open('POST', 'www.avatar.com', false);

		xhr.send();
	}
})()
